﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class GBNHZinit : MonoBehaviour {

    public bool testAds;

	void Start() {
		HeyzapAds.start("c31b131c85b04c80b6950981635c1ff24ec843c781129730479aef64e3bbfcda", HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);
        Debug.Log("HEYZAP IS LOADED");
        if (testAds) {
            HeyzapAds.ShowMediationTestSuite();
        }
        HZInterstitialAd.Fetch();
    }
}
