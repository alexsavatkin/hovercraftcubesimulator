﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class GBNHZshowReward : MonoBehaviour {

    public GameObject btn;
    public int rewardType;
    bool showOnEnable = true;
    float loadtime = 3.5f;
    float updatetime = 10000.0f;
    bool isPause;
    float t;

    void Awake() {
        loadtime = 3.5f;
        updatetime = 10000.0f;
        ShowReward();
    }

    void Start() {
        HZIncentivizedAd.AdDisplayListener listener = delegate (string adState, string adTag) {
            if (adState.Equals("show")) {
                PauseOn();
            }
            if (adState.Equals("hide")) {
                PauseOff();
                // Здесь добавить БОНУС за просмотр рекламы
                if (rewardType == 0) {
                    GameObject.Find("CanvasGame").GetComponent<GameManager>().ScoresOnCurrentLevel += 200;
                    PlayerPrefs.SetInt("scores", GameObject.Find("CanvasGame").GetComponent<GameManager>().ScoresOnCurrentLevel);
                } else if (rewardType == 1) {
                    // Тип бонуса 2
                }
            }
            if (adState.Equals("click")) {

            }
            if (adState.Equals("failed")) {

            }
            if (adState.Equals("available")) {

            }
            if (adState.Equals("fetch_failed")) {

            }
            if (adState.Equals("audio_starting")) {

            }
            if (adState.Equals("audio_finished")) {

            }
        };

        HZIncentivizedAd.SetDisplayListener(listener);
    }

    void PauseOn() {
        if (Time.timeScale == 0) {
            isPause = true;
        }
        Time.timeScale = 0.0f;
        AudioListener.volume = 0.0f;
    }

    void PauseOff() {
        Time.timeScale = 1.0f;
        if (isPause) {
            Time.timeScale = 0;
            isPause = false;
        }
        AudioListener.volume = 1.0f;
    }

    public void ShowReward() {
        t = loadtime;
        HZIncentivizedAd.Fetch();
    }

    void Update() {
        t -= Time.unscaledDeltaTime;
        if (t < 2) {
            if (t < 0) {
                btn.GetComponent<GBNHZbtnReward>().EndLoading();
                Destroy(gameObject);
            }
            if (HZIncentivizedAd.IsAvailable()) {
                t = updatetime;
                HZIncentivizedAd.Show();
                btn.GetComponent<GBNHZbtnReward>().EndLoading();
                Destroy(gameObject);
            }
        }
    }
}