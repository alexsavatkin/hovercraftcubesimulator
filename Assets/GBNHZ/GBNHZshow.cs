﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class GBNHZshow : MonoBehaviour {

    private static GBNHZshow _instance;
    public static GBNHZshow Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GBNHZshow>();
                if (_instance == null)
                {
                    GameObject go = new GameObject("GBNHZshow");
                    _instance = go.AddComponent<GBNHZshow>();
                }
            }
            return _instance;
        }
    }

    public bool showOnEnable = true;
    public float loadtime = 1.0f;
    public float updatetime = 120.0f;

    public float t;

    bool isPause;

    void Awake() {
        HZInterstitialAd.AdDisplayListener listener = delegate (string adState, string adTag) {
            if (adState.Equals("show")) {
                PauseOn();
            }
            if (adState.Equals("hide")) {
                PauseOff();
                HZInterstitialAd.Fetch();
            }
            if (adState.Equals("click")) {

            }
            if (adState.Equals("failed")) {

            }
            if (adState.Equals("available")) {

            }
            if (adState.Equals("fetch_failed")) {

            }
            if (adState.Equals("audio_starting")) {

            }
            if (adState.Equals("audio_finished")) {

            }
        };

        HZInterstitialAd.SetDisplayListener(listener);
    }

    void OnEnable() {
        t = updatetime;
        if (showOnEnable) {
            Show();
        }
        HZInterstitialAd.Fetch();
    }

    void PauseOn() {
        if (Time.timeScale == 0) {
            isPause = true;
        }
        Time.timeScale = 0.0f;
        AudioListener.volume = 0.0f;
    }

    void PauseOff() {
        Time.timeScale = 1.0f;
        if (isPause) {
            Time.timeScale = 0;
            isPause = false;
        }
        AudioListener.volume = 1.0f;
    }

    public void Show() {
        Debug.Log("Show ADS");
        t = loadtime;
    }

    public void ShowNow() {
        if (PlayerPrefs.HasKey("ADS"))
        {
            if (PlayerPrefs.GetInt("ADS") == 0)
            {
                Debug.Log("Show ADS");
                PlayerPrefs.SetInt("ADS", 1);
                if (HZInterstitialAd.IsAvailable())
                {
                    t = updatetime;
                    HZInterstitialAd.Show();
                }
            }
            else
            {
                PlayerPrefs.SetInt("ADS", 0);
            }
        }
        else
        {
            Debug.Log("Show ADS");
            PlayerPrefs.SetInt("ADS", 1);
            if (HZInterstitialAd.IsAvailable())
            {
                t = updatetime;
                HZInterstitialAd.Show();
            }
        }
    }

    void Update() {
        t -= Time.unscaledDeltaTime;
        if (t < 0) {
            if (HZInterstitialAd.IsAvailable()) {
                t = updatetime;
                HZInterstitialAd.Show();
            }
            if (t < -loadtime * 1.2f) {
                t = updatetime;
            }
        }
    }
}
