﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GBNHZbtnReward : MonoBehaviour {

    public GameObject rewardedVideo;
    public Text text;
    public bool loading;
    public string TextErn = "Earn 500 gold";
    public string TextLoading = "Loading...";
    public int rewardType = 0;

    public void EndLoading() {
        loading = false;
        text.text = TextErn;
    }

    public void ShowRewardedVideo() {
        if (!loading) {
            loading = true;
            text.text = TextLoading;
            GameObject go = (Instantiate(rewardedVideo) as GameObject);
            go.GetComponent<GBNHZshowReward>().btn = gameObject;
            go.GetComponent<GBNHZshowReward>().rewardType = rewardType;
        }
    }
}
