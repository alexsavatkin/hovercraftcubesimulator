﻿using UnityEngine;
using System.Collections;

public class BtnUrl : MonoBehaviour {
	public bool isMoreGames;
#if UNITY_IOS
	public int appID;
#endif

    public void OpenGameURL() {
		if (!isMoreGames) {
#if UNITY_IOS
			// Apple iTunes
			Application.OpenURL ("http://itunes.apple.com/app/id" + appID.ToString());
#else
			if (Application.companyName != "Amazing Games" && Application.companyName != "AmazingGames") {
				// Google Review App
				Application.OpenURL ("market://details?id=" + Application.bundleIdentifier);
			} else {
				// Amazon Review App
				Application.OpenURL ("http://www.amazon.com/gp/mas/dl/android?p=" + Application.bundleIdentifier);
			}
#endif
		} else {
#if UNITY_IOS
			// Apple iTunes
			if (Application.companyName == "GBN") {
                Application.OpenURL ("https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?mt=8&path=apps%2fgamesbannernetwork");
				//Application.OpenURL ("appstore.com/gamesbannernetwork");
			} else if (Application.companyName.Trim() == "TaigaGames" || Application.companyName.Trim == "Tayga Games" || Application.companyName == "TaygaGames") {
				Application.OpenURL ("https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?mt=8&path=apps%2ftaygagamesooo");
                //Application.OpenURL ("appstore.com/taygagamesooo");
			}
#else
            if (Application.companyName.Trim() == "AmazingGames") {
				Application.OpenURL ("http://www.amazon.com/s/ref=bl_sr_mobile-apps?_encoding=UTF8&field-brandtextbin=Amazing%203D%20Games&node=2350149011");
			} else if (Application.companyName.Trim() == "PlayMechanics") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=PlayMechanics");
			} else if (Application.companyName.Trim() == "GamesArcade") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=GamesArcade");
			} else if (Application.companyName.Trim() == "TriggerTeam") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=Trigger%20Team");
			} else if (Application.companyName.Trim() == "GBN") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=GBN,+Llc");
			} else if (Application.companyName.Trim() == "MobileHero") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=MobileHero");
			} else if (Application.companyName.Trim() == "MyPocketGames") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=MyPocketGames");
			} else if (Application.companyName.Trim() == "BrosGames") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=BrosGames");
			} else if (Application.companyName.Trim() == "ClickBangPlay" || Application.companyName.Trim() == "ClickBang") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=ClickBangPlay");
			} else if (Application.companyName.Trim() == "TaigaGames") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=TaigaGames");
			} else if (Application.companyName.Trim() == "CartoonWorldGames") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=Cartoon+World+Games");
			} else if (Application.companyName.Trim() == "BigMadGames") {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=Big%20Mad%20Games");
			} else if (Application.companyName.Trim() == "PixelIsland") {
                Application.OpenURL("https://play.google.com/store/apps/developer?id=Pixel%20Island");
            }
            else if (Application.companyName.Trim() == "WorldOfCubes")
            {
                Application.OpenURL("https://play.google.com/store/apps/developer?id=World%20of%20Cubes");
            }
#endif
        }
    }
}