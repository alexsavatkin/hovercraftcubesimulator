﻿using UnityEngine;
using System.Collections;

public class ShowHide : MonoBehaviour {
	
	public GameObject[] menu;
	public GameObject btn1;
	public GameObject btn2;
	
	bool b;

	int i;

	void Update () {
		b = false;
		foreach (GameObject g in menu) {
			if (g.activeSelf)
				b = true;
		}
		
		if (b) {
			//i++;
			//if (i > 2) {
				if (!btn1.activeSelf)
					btn1.SetActive (true);
				if (!btn2.activeSelf)
					btn2.SetActive (true);
			//}
		} else {
			//i = 0;
			if (btn1.activeSelf)
				btn1.SetActive (false);
			if (btn2.activeSelf)
				btn2.SetActive (false);
		}
		
	}
}