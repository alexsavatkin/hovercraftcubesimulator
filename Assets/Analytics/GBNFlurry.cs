using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Analytics;
using Debug = UnityEngine.Debug;

public enum AppType 
{
	GooglePlay_iOsFree,
	Amazon_iOsFull
}

public class GBNFlurry : MonoBehaviour
{
    private static GBNFlurry _instance;
    public static GBNFlurry Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GBNFlurry>();
                if (_instance == null)
                {
                    GameObject go = new GameObject("GBNFlurry");
                    _instance = go.AddComponent<GBNFlurry>();
                }
            }
            return _instance;
        }
    }

    [Header("Flurry Settings")]
    public AppType appType;
    [SerializeField]
    private string _iosFullApiKey = string.Empty;
    [SerializeField]
    private string _iosFreeApiKey = string.Empty;
    [SerializeField]
    private string _androidGooglePlayApiKey = string.Empty;
    [SerializeField]
    private string _androidAmazonApiKey = string.Empty;

    /// <summary>
    /// Create Flurry singleton instance and log single event.
    /// </summary>
    private void Awake()
    {
        IAnalytics service = Flurry.Instance;

        AssertNotNull(service, "Unable to create Flurry instance!", this);
        if (appType == AppType.GooglePlay_iOsFree)
        {
            Assert(!string.IsNullOrEmpty(_androidGooglePlayApiKey), "_androidApiKey is empty!", this);
            Assert(!string.IsNullOrEmpty(_iosFreeApiKey), "_iosApiKey is empty!", this);
        }
        else
        {
            Assert(!string.IsNullOrEmpty(_androidAmazonApiKey), "_androidApiKey is empty!", this);
            Assert(!string.IsNullOrEmpty(_iosFullApiKey), "_iosApiKey is empty!", this);
        }

        service.SetLogLevel(LogLevel.All);
        if (appType == AppType.GooglePlay_iOsFree)
        {
            service.StartSession(_iosFreeApiKey, _androidGooglePlayApiKey);
        }
        else
        {
            service.StartSession(_iosFullApiKey, _androidAmazonApiKey);
        }
    }

    // GAME EVENTS
    public void pSetEventNameKeyVal(string name, string key, string val)
    {
        Flurry.Instance.LogEvent(name, new Dictionary<string, string> { { key, val } });
    }
    public void pSetEventKeyVal(string key, string val)
    {
        Flurry.Instance.LogEvent(key, new Dictionary<string, string> { { key, val } });
    }
    public void pSetEventStr(string str)
    {
        Flurry.Instance.LogEvent(str, new Dictionary<string, string> { { str, str } });
    }
    public void pBeginTimedEvent(string str)
    {
        Flurry.Instance.BeginLogEvent(str);
    }
    public void pEndTimedEvent(string str)
    {
        Flurry.Instance.EndLogEvent(str);
    }

    //public void pSetEventInt(int x) {
    //Flurry.Instance.LogEvent("event", new Dictionary<string, string> { { key, key } });
    //}


    void Start()
    {
#if UNITY_ANDROID
        FlurryAndroid.SetLogEnabled(true);
#endif
#if UNITY_IOS
        FlurryIOS.SetDebugLogEnabled(true);
#endif
        if (Application.loadedLevelName == "game")
        {
            Flurry.Instance.BeginLogEvent("timed-event");
        }
    }


    /// <summary>
    /// Draw GUI controls and call analytics service.
    /// </summary>
    /*
	private void OnGUI()
	{
		int offset = 0;
		IAnalytics service = Flurry.Instance;

		if (Button("Log User Name", offset++))
		{
			service.LogUserID("Github User");
		}

		if (Button("Log User Age", offset++))
		{
			service.LogUserAge(24);
		}

		if (Button("Log User Gender", offset++))
		{
			service.LogUserGender(UserGender.Male);
		}
		
		if (Button("Log User Location", offset++))
		{
			//TODO: impl
		}

		if (Button("Log Event", offset++))
		{
			service.LogEvent("event", new Dictionary<string, string>
			{
#if UNITY_5
			    { "AppVersion", Application.version },
#endif
                { "UnityVersion", Application.unityVersion }
			});
		}

		if (Button("Begin Timed Event", offset++))
		{
			service.BeginLogEvent("timed-event");
		}

		if (Button("End Timed Event", offset++))
		{
			service.EndLogEvent("timed-event");
		}

		if (Button("Log Page View", offset++))
		{
			//TODO: impl
		}
		
		if (Button("Log Error", offset))
		{
			service.LogError("test-script-error", "Test Error", this);
		}
	}

	private bool Button(string label, int index)
	{
		float width = Screen.width * 0.7f;
		float height = Screen.height * 0.1f;

		Rect rect = new Rect(Screen.width * 0.5f - width * 0.5f, 
		                     height * index * 1.1f,
		                     width,
		                     height);

		return GUI.Button(rect, label);
	}
	*/

    #region [Assert Methods]
    [Conditional("UNITY_EDITOR")]
    private void Assert(bool condition, string message, Object context)
    {
        if (condition)
        {
            return;
        }

        Debug.LogError(message, context);
    }

    [Conditional("UNITY_EDITOR")]
    private void AssertNotNull(object target, string message, Object context)
    {
        Assert(target != null, message, context);
    }
    #endregion
}
