﻿using UnityEngine;
using System.Collections;

public class SendNotifByTimer : MonoBehaviour {

    public bool enableSound = false;
    public string str1;
    public string str2;
    public string str3;
    ELANNotification notif;

    float timer = 1.0f;

    void SendNotif(int ind) {
        if (ind == 0) {
            notif = new ELANNotification();
            notif.message = Application.productName;
            notif.title = str1;
            notif.repetitionTypeTime = EnumTimeType.Days;
            notif.repetition = 9;
            notif.delayTypeTime = EnumTimeType.Days;
            notif.delay = 1;
            notif.useSound = enableSound;
            notif.soundName = "bing";
            notif.useVibration = true;
            notif.send();
        } else if (ind == 1) {
            notif = new ELANNotification();
            notif.message = Application.productName;
            notif.title = str2;
            notif.repetitionTypeTime = EnumTimeType.Days;
            notif.repetition = 9;
            notif.delayTypeTime = EnumTimeType.Days;
            notif.delay = 4;
            notif.useSound = enableSound;
            notif.soundName = "bing";
            notif.useVibration = true;
            notif.send();
        } else if (ind == 2) {
            notif = new ELANNotification();
            notif.message = Application.productName;
            notif.title = str3;
            notif.repetitionTypeTime = EnumTimeType.Days;
            notif.repetition = 9;
            notif.delayTypeTime = EnumTimeType.Days;
            notif.delay = 7;
            notif.useSound = enableSound;
            notif.soundName = "bing";
            notif.useVibration = true;
            notif.send();
        }
    }

    bool b;

    void Awake() {
        b = true;
        timer = 1.0f;
        ELANManager.CancelRepeatingNotification();
        ELANManager.CancelAllNotifications();
    }

	void Update () {
        if (b) {
            timer -= Time.deltaTime;
            if (timer < 0) {
                SendNotif(0);
                SendNotif(1);
                SendNotif(2);
                b = false;
            }
        }
	}
}
