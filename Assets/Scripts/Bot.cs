﻿using UnityEngine;
using System.Collections;

public class Bot : MonoBehaviour {

    public float distFollow = 50;
    public float speed = 1.0f;
    
    Transform tr;
    float d;
    public float sp;

    void Awake()
    {
        transform.localScale *= 1.3f;
        distFollow = 50.0f;
        speed = 10.0f;
        tr = GameObject.Find("CubeMain").transform;
        transform.position += new Vector3(0, 1, 0);
    }

    void Update ()
    {
        d = Vector3.Distance(tr.position, transform.position);
        if (d < distFollow)
        {
            sp = -(distFollow - d) / distFollow;
            transform.Translate(0, Time.deltaTime * sp * speed, 0);
        }
	}
}
