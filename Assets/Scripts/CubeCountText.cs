﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CubeCountText : MonoBehaviour {

    public int n;
    public Creator crt;
    Text txt;

    void Awake()
    {
        txt = GetComponent<Text>();
    }

    void Update ()
    {
        txt.text = crt.CubeCountsText[n].text;
    }
}
