﻿using UnityEngine;
using System.Collections;

public class AnimRotation : MonoBehaviour
{
    public float speed = 10;
    public Vector3 dir = new Vector3(0, 1, 0);

    void Update()
    {
        transform.Rotate(speed * Time.deltaTime * dir);
    }
}
