﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scores : MonoBehaviour {

    public GameManager gm;

    Text txt;

    void Awake()
    {
        txt = GetComponent<Text>();
    }

    void Update()
    { 
        txt.text =  gm.ScoresOnCurrentLevel.ToString();    
    }
}
