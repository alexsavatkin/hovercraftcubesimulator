﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
    private static Main _instance;
    public static Main Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Main>();
                if (_instance == null)
                {
                    GameObject go = new GameObject("Main");
                    _instance = go.AddComponent<Main>();
                }
            }
            return _instance;
        }
    }

    public int scores = 0;
    public int health = 100;
    public int speed = 100;
    public GameObject explosionFx;
    public GameObject[] bots;

    void Update()
    {
        
    }
}
