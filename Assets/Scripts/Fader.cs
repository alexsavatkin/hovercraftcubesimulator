﻿using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public float Timer = 1;
    public bool IsUnscaledDeltaTime;

    float t;
    Image img;
    float a;

    void Awake()
    {
        t = Timer;
        img = GetComponent<Image>();    
    }

    void OnEnable()
    {
        Timer = t;
    }

    void Update()
    {
        if (IsUnscaledDeltaTime)
        {
            Timer -= Time.unscaledDeltaTime;
        }
        else
        {
            Timer -= Time.deltaTime;
        }
        img.color = new Color(img.color.r, img.color.g, img.color.b, Timer / t);
        if (Timer < 0)
        {
            Timer = t;
            gameObject.SetActive(false);
        }
    }

}
