﻿using UnityEngine;
using System.Collections;

public class Swipe : MonoBehaviour
{
    public bool isOn;
    public float dx;
    public float dy;

    float mouseCurrX = 0;
    float mousePrevX = 0;
    float mouseCurrY = 0;
    float mousePrevY = 0;

    float dt;

    void Update()
    {
        if (Time.timeScale > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                dt = 1000.0f;
                isOn = true;
                dy = 0;
                mouseCurrX = Input.mousePosition.x;
                mousePrevX = mouseCurrX;
                mouseCurrY = Input.mousePosition.y;
                mousePrevY = mouseCurrY;
            }

            if (Input.GetMouseButtonUp(0))
            {
                isOn = false;
                dy = 0;
                dx = 0;
            }

            if (dt < 0)
            {
                dt = 0.0f;
                isOn = false;
            }
            else
            {
                dt -= Time.deltaTime;
                if (isOn)
                {
                    mousePrevX = mouseCurrX;
                    mouseCurrX = Input.mousePosition.x;
                    dx = mouseCurrX - mousePrevX;
                    mousePrevY = mouseCurrY;
                    mouseCurrY = Input.mousePosition.y;
                    dy = mouseCurrY - mousePrevY;
                }
            }

            dx *= Time.deltaTime;
            dy *= Time.deltaTime;
        }
    }
}