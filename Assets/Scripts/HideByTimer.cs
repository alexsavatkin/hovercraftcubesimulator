﻿using UnityEngine;
using System.Collections;

public class HideByTimer : MonoBehaviour
{
    public bool IsDestroy;
    public float Timer = 1.0f;
    public bool IsUnscaledDeltaTime;

    float t;

    void Awake()
    {
        t = Timer;
    }

    void Update()
    {
        if (t > 0)
        {
            if (IsUnscaledDeltaTime)
            {
                t -= Time.unscaledDeltaTime;
            }
            else
            {
                t -= Time.deltaTime;
            }
            if (t <= 0)
            {
                t = Timer;
                if (IsDestroy)
                {
                    Destroy(gameObject);
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}