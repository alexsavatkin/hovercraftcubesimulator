﻿using UnityEngine;
using System.Collections;

public class RandomCube : MonoBehaviour {

    public Rigidbody rb;
    float t;

	void Start () {
        //rb = go.GetComponent<Rigidbody>();
        rb.AddForce(Random.Range(-400, 400), Random.Range(-100, 500), Random.Range(-200, 200));
        rb.AddTorque(Random.Range(-150, 150), Random.Range(-150, 150), Random.Range(-150, 150));
	}

    void Update()
    {
        t += Time.deltaTime;
        if (t > 2.0f)
        {
            Destroy(gameObject);
        }
    }
}
