﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] Parts;
    public int Length = 10;
    public GameObject Wolrd;
    public GameObject Garazh;
    public GameObject GameGUI;
    public GameObject GameMainGUI;
    public Transform StartPosition;
    public Transform Car;
    public GameObject CameraGarazh;
    public GameObject CameraWorld;
    public Creator creator;

    [Header("Player")]
    public GameObject[] AllCubes;

    public PlayerController plcc;

    float loadTimer;

    void Awake()
    {
        //GenerateLevel();
        loadTimer = -1.0f;
    }

    void Update()
    {
        if (loadTimer > 0)
        {
            loadTimer -= Time.deltaTime;
            if (loadTimer <= 0)
            {
                loadTimer = -1;
                GenerateLevel();
            }
        }
    }

    public void SetLoadTimer()
    {
        loadTimer = 2.0f;
    }

    public void GenerateLevel()
    {
        creator.CurrentSlot = 0;
        creator.saver.Save();
        creator.musicInEditor.Stop();
        creator.musicInGame.Play();
        Car = GameObject.Find("CubeMain").transform;

        AllCubes = GameObject.FindGameObjectsWithTag("HCCube");
        for (int i = 0; i < AllCubes.Length; i++)
        { 
            if (AllCubes[i].name != "CubeMain")
            {
                //AllCubes[i].GetComponent<Rigidbody>().isKinematic = true;
                //AllCubes[i].GetComponent<Rigidbody>().useGravity = false;
                AllCubes[i].GetComponent<HCCube>().InGame = true;
                AllCubes[i].GetComponent<HCCube>().FindPlayerController(plcc);
                AllCubes[i].GetComponent<HCCube>().trmain = Car;
                
                AllCubes[i].GetComponent<BoxCollider>().isTrigger = true;
                Destroy(AllCubes[i].GetComponent<Rigidbody>());
            }
        }

        
        Car.GetComponent<HCCube>().IsMain = true;
        //Car.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
        Car.GetComponent<Rigidbody>().isKinematic = false;
        Car.GetComponent<Rigidbody>().useGravity = true;
        Car.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;// | RigidbodyConstraints.FreezeRotationY;

        Car.position = StartPosition.position;
        Car.rotation = StartPosition.rotation;
        Car.transform.localScale *= 0.8f;

        GameMainGUI.SetActive(true);
        Wolrd.SetActive(true);
        GameGUI.SetActive(false);
        Garazh.SetActive(false);
        CameraGarazh.SetActive(false);
        CameraWorld.SetActive(true);
        Destroy(GameGUI);
        Destroy(Garazh);
        Destroy(CameraGarazh);
        GameObject g = (GameObject)Instantiate(Parts[0], transform.position, transform.rotation);
        g.transform.parent = transform;
        for (int i = 1; i < Length; i++)
        {
            g = (GameObject)Instantiate(Parts[Random.Range(0, Parts.Length)], g.GetComponent<RoadSegment>().trOut.position, g.GetComponent<RoadSegment>().trOut.rotation);
            g.transform.parent = transform;
        }
    } 
}
