﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SwipeCamera : MonoBehaviour
{
    public float SpeedY = 10.0f;
    public float SpeedX = 10.0f;
    public Transform RotY;
    public Transform RotX;
    public float RangeX = 75;
    public Slider slider;
    public Camera cam;

    Swipe swipe;
    float h;
    float v;

    void Awake()
    {
        swipe = GetComponent<Swipe>();
    }

    void Update()
    {
        cam.fieldOfView = 70 - slider.value * 30;
        h = swipe.dx;
        v = swipe.dy;
        RotX.Rotate(-v * SpeedX, 0, 0);
        if (RotX.rotation.eulerAngles.x > RangeX && RotX.rotation.eulerAngles.x < 90)
        {
            RotX.rotation = Quaternion.Euler(RangeX, RotX.rotation.eulerAngles.y, RotX.rotation.eulerAngles.z);
        }
        else if (RotX.rotation.eulerAngles.x < 360 - RangeX && RotX.rotation.eulerAngles.x > 270)
        {
            RotX.rotation = Quaternion.Euler(360 - RangeX, RotX.rotation.eulerAngles.y, RotX.rotation.eulerAngles.z);
        }
        RotY.Rotate(0, h * SpeedY, 0);
    }
}
