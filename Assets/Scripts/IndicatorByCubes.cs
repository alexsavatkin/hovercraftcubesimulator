﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IndicatorByCubes : MonoBehaviour {

    public bool isFuel;
    public Creator creator;
    public int engines;
    public int allcubes;
    int maxCubes;
    Image img;
    int kkk;

    void Awake()
    {
        maxCubes = 300;
        img = GetComponent<Image>();
    }

    void Update()
    {
        allcubes = creator.length;
        engines = creator.engines;
        if (isFuel)
        {
            img.fillAmount = (float)creator.length / (float)maxCubes;
        }
        else
        {
            if (creator.engines == 1)
            {
                img.fillAmount = 3.0f / 7.0f;
            }
            else if (creator.engines == 2)
            {
                img.fillAmount = 4.0f / 7.0f;
            }
            else if (creator.engines == 3)
            {
                img.fillAmount = 5.0f / 7.0f;
            }
            else if (creator.engines == 4)
            {
                img.fillAmount = 6.0f / 7.0f;
            }
            else if (creator.engines >= 5)
            {
                img.fillAmount = 7.0f / 7.0f;
            }
            else
            {
                img.fillAmount = 2.5f / 7.0f;
            }
        }
    }
}
