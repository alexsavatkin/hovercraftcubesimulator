﻿using UnityEngine;
using UnityEngine.UI;

public class FakeLoading : MonoBehaviour
{
    public float FakeLoadingTime = 3.0f;
    public Image ProgressBar;
    public Text Progress;
    float percent;
    float time;
    float speed;
    float fs;

    void Awake()
    {
        time = 0f;
    }

    void Update()
    {
        fs += Time.deltaTime * Random.Range(0.25f, 1.5f);
        if (fs > 1.0f)
        {
            speed = Random.Range(0.1f, 3.0f);
            fs = 0;
        }
        time += Time.deltaTime * speed;
        percent = time / FakeLoadingTime;
        ProgressBar.fillAmount = percent;
        Progress.text = "LOADING..." + ((int)(100 * percent)).ToString() + "%";
        if (time > FakeLoadingTime)
        {
            Application.LoadLevel("Menu");
        }
    }
}