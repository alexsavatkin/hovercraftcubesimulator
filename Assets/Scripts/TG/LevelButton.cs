﻿using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int LevelNumber;             // Номер уровня
    public Text TextLevelNumber;        // Номер уровня - текст
    public Sprite SpriteButtonOpened;  // Подложка открытого уровня
    public Sprite SpriteButtonLocked; // Подложка закрытого уровня
    public GameObject[] StarsOpened;    // Звезды-объекты открытого уровня
    public GameObject[] StarsLocked;    // Звезды-объекты закрытого уровня

    // Загрузка уровня по номеру
    public void LoadLevel()
    {
        Time.timeScale = 1.0f;
        PlayerPrefs.SetInt("levelCurrent", LevelNumber);
        Debug.Log("<color=yellow><b>Уровень "
            + LevelNumber.ToString()
            + " загружен</b></color>");
        Application.LoadLevel("Game");
    }

    // Установка вида и показ/скрытие лишних объектов
    // -1 - уровень закрыт, 0, 1, 2, 3 - количество звезд
    public void SetStars(int n)
    {
        if (n == -1)
        {
            GetComponent<Image>().sprite = SpriteButtonLocked;
            for (int i = 0; i < 3; i++)
            {
                StarsOpened[i].SetActive(false);
                StarsLocked[i].SetActive(false);
            }
            GetComponent<Button>().enabled = false;
            TextLevelNumber.text = "";
        }
        else
        {
            GetComponent<Image>().sprite = SpriteButtonOpened;
            for (int i = 0; i <= n - 1; i++)
            {
                StarsLocked[i].SetActive(false);
            }
            for (int i = 0; i < n; i++)
            {
                StarsOpened[i].SetActive(true);
            }
            for (int i = n; i < 3; i++)
            {
                StarsOpened[i].SetActive(false);
            }
            TextLevelNumber.text = LevelNumber.ToString();
        }
    }
}