﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [Header("Игровые параметры")]
    public int ScoresOnCurrentLevel;    // количество очков на текущем уровне

    [Header("GUI")]
    public GameObject PopupPause;
    public GameObject PopupWin;
    public GameObject PopupLose;
    public GameObject PopupShop;
    public GameObject[] StarsEnabled;
    public GameObject[] StarsDisabled;
    public Text TextScores;

    [Header("Главная камера")]
    public Camera CameraMain;
    public AudioListener CameraAudioListener;
    public GameObject SwipeCamera;
    public GameObject GameGUI;

    [Header("Звуки")]
    //public AudioSource SndClick;

    float ftimer;

    // Переход по ссылке
    public void OpenGameURL(string sURL)
    {
        Application.OpenURL(sURL);
    }

    // Перезагрузка уровня
    public void ButtonRestart()
    {
        Time.timeScale = 1;
        Application.LoadLevel("Game");
    }

    // Загрузка следующего уровня
    public void ButtonNextLevel()
    {
        Debug.Log("<color=yellow><b>Уровень "
            + (PlayerPrefs.GetInt("levelCurrent") + 1).ToString()
            + " загружен</b></color>");
        Time.timeScale = 1;
        if (PlayerPrefs.GetInt("levelCurrent") < PlayerPrefs.GetInt("levelMax"))
        {
            PlayerPrefs.SetInt("levelCurrent", PlayerPrefs.GetInt("levelCurrent") + 1);
        }
        Application.LoadLevel("Game");
    }

    // Воспроизведение звука в проекте по названию
    public void SoundPlay(string s)
    {
        GameObject.Find(s).GetComponent<AudioSource>().Play();
    }

    // Загрузка главного меню
    public void ButtonMainMenu()
    {
        Time.timeScale = 1;
        Application.LoadLevel("Menu");
    }

    // Пауза
    public void ButtonPause(bool pauseOn)
    {
        if (pauseOn)
        {
            Time.timeScale = 0;
            PopupPause.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            PopupPause.SetActive(false);
        }
    }

    // Победа
    public void Win()
    {
        Time.timeScale = 0;
        int stars = 0;
        //PlayerPrefs.SetInt("scores", PlayerPrefs.GetInt("scores") + ScoresOnCurrentLevel);
        PopupWin.SetActive(true);
        if (ScoresOnCurrentLevel >= 300)
        {
            stars = 3;
        }
        else if (ScoresOnCurrentLevel >= 200)
        {
            stars = 2;
        }
        else
        {
            stars = 1;
        }
        for (int i = 0; i < stars; i++)
        {
            StarsEnabled[i].SetActive(true);
            StarsDisabled[i].SetActive(false);
        }
        for (int i = stars; i < 3; i++)
        {
            StarsEnabled[i].SetActive(false);
            StarsDisabled[i].SetActive(true);
        }
        if (PlayerPrefs.GetInt("levelStars" + PlayerPrefs.GetInt("levelCurrent").ToString()) < stars)
        {
            PlayerPrefs.SetInt("levelStars" + PlayerPrefs.GetInt("levelCurrent").ToString(), stars);
        }
    }

    // Проигрыш
    public void Lose()
    {
        Time.timeScale = 0;
        PlayerPrefs.SetInt("scores", ScoresOnCurrentLevel);
        PopupLose.SetActive(true);
        GameObject.Find("GBNHZshow").GetComponent<GBNHZshow>().ShowNow();
    }

    void Awake()
    {
        Time.timeScale = 1;
        ScoresOnCurrentLevel = 0;
        ScoresOnCurrentLevel = PlayerPrefs.HasKey("scores") ? PlayerPrefs.GetInt("scores") : 0;
#if TEST_VERSION
        ScoresOnCurrentLevel = PlayerPrefs.HasKey("scores") ? PlayerPrefs.GetInt("scores") : 10000;
#endif
        PlayerPrefs.SetInt("scores", ScoresOnCurrentLevel);
        CameraAudioListener.enabled = PlayerPrefs.GetInt("sound") == 1 ? true : false;
    }

    /*
    void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.W))
        {
            Win();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Lose();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Main.Instance.scores += 50;
            ScoresOnCurrentLevel += 50;
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            Main.Instance.scores -= 50;
            ScoresOnCurrentLevel -= 50;
        }
        //TextScores.text = ScoresOnCurrentLevel.ToString();
        ftimer += Time.unscaledDeltaTime;
    }
    */
}
