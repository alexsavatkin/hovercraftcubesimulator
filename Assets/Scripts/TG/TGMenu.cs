﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TGMenu : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("TGMenu/Удалить все сохранения")]
    private static void DeleteAllSaves()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("<color=red><b>Все сохранения удалены</b></color>");
    }

    [MenuItem("TGMenu/Открыть все уровни с 3-мя звездами")]
    private static void OpenAllLevels3Stars()
    {
        PlayerPrefs.SetInt("levelOpened", PlayerPrefs.GetInt("levelMax"));
        for (int i = 1; i <= PlayerPrefs.GetInt("levelMax"); i++)
        {
            PlayerPrefs.SetInt("levelStars" + i.ToString(), 3);
        }
        Debug.Log("<color=red><b>Все уровни открыты с 3-мя звездами</b></color>");
    }

    [MenuItem("TGMenu/Открыть все уровни с 1-ой звездой")]
    private static void OpenAllLevels1Stars()
    {
        PlayerPrefs.SetInt("levelOpened", PlayerPrefs.GetInt("levelMax"));
        for (int i = 1; i <= PlayerPrefs.GetInt("levelMax"); i++)
        {
            PlayerPrefs.SetInt("levelStars" + i.ToString(), 1);
        }
        Debug.Log("<color=red><b>Все уровни открыты с 1-ой звездой</b></color>");
    }
#endif
}