﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : MonoBehaviour
{
    [Header("Игровые параметры")]
    public int LevelMax = 6;    // количество уровней в игре

    [Header("GUI")]
    public GameObject PopupMainMenu;
    public GameObject PopupLevels;
    public GameObject PopupOptions;
    public GameObject TextLoading;
    public GameObject TextLoadingLevel;

    [Header("Кнопка Звук Вкл/Выкл")]
    public Image ImageSoundButton;
    public Sprite SoundEnabled;
    public Sprite SoundDisabled;

    [Header("Кнопки выбора уровня")]
    public GameObject PanelLevels;
    public GameObject LevelButton;
    public LevelButton[] LevelButtons;

    [Header("Главная камера")]
    public Camera CameraMain;
    public AudioListener CameraAudioListener;

    public GameObject Loading;
    [Header("Звуки")]
    //public AudioSource sndClick;

    float ftimer;

    // Переход по ссылке
    public void OpenGameURL(string sURL)
    {
        Application.OpenURL(sURL);
    }

    // Выход из игры
    public void btnQuit()
    {
        Application.Quit();
    }

    // Кнопка Старт
    public void btnStart()
    {
        Time.timeScale = 1;
        if (PlayerPrefs.GetInt("levelMax") == 1)
        {
            PlayerPrefs.SetInt("levelCurrent", 1);
            TextLoading.SetActive(true);
            Application.LoadLevel("Game");
        }
        else
        {
            PopupMainMenu.SetActive(false);
            PopupLevels.SetActive(true);
            //LoadLevelButtons();
        }
    }

    // Загрузка уровня N
    public void btnLevel(int n)
    {
        Debug.Log("<color=yellow><b>Уровень " + n.ToString() + " загружен</b></color>");
        Time.timeScale = 1;
        PlayerPrefs.SetInt("levelCurrent", n);
        TextLoadingLevel.SetActive(true);
        Application.LoadLevel("Game");
    }

    //  Загрузка параметров 
    public void PlayerPrefsLoad()
    {
        PlayerPrefs.SetInt("levelMax", LevelMax);
        if (!PlayerPrefs.HasKey("sound"))
        {
            PlayerPrefs.SetInt("sound", 1);
        }
        if (!PlayerPrefs.HasKey("scores"))
        {
            PlayerPrefs.SetInt("scores", 0);
        }
        if (!PlayerPrefs.HasKey("levelCurrent"))
        {
            PlayerPrefs.SetInt("levelCurrent", 0);
        }
        if (!PlayerPrefs.HasKey("levelStars1"))
        {
            PlayerPrefs.SetInt("levelStars1", 0);
        }
        for (int i = 1; i <= LevelMax; i++)
        {
            if (!PlayerPrefs.HasKey("levelStars" + i.ToString()))
            {
                PlayerPrefs.SetInt("levelStars" + i.ToString(), -1);
            }
            else if (PlayerPrefs.GetInt("levelStars" + i.ToString()) > 0
              && i != PlayerPrefs.GetInt("levelMax")
              && PlayerPrefs.GetInt("levelStars" + (i + 1).ToString()) == -1)
            {
                PlayerPrefs.SetInt("levelStars" + (i + 1).ToString(), 0);
            }
        }
        if (LevelMax != 1)
        {
            LevelButtons = new LevelButton[LevelMax];
            LevelButtons[0] = LevelButton.GetComponent<LevelButton>();
            for (int i = 2; i <= LevelMax; i++)
            {
                GameObject g = Instantiate(LevelButton);
                g.transform.parent = PanelLevels.transform;
                g.name = "btnLevel" + i.ToString();
                g.GetComponent<LevelButton>().LevelNumber = i;
                g.GetComponent<LevelButton>().TextLevelNumber.text = i.ToString();
                g.transform.localScale = new Vector3(1, 1, 1);
                LevelButtons[i - 1] = g.GetComponent<LevelButton>();
            }
            LoadLevelButtons();
        }
        CameraAudioListener.enabled = PlayerPrefs.GetInt("sound") == 1 ? true : false;
    }

    public void LoadLevelButtons()
    {
        for (int i = 0; i < LevelMax; i++)
        {
            LevelButtons[i].SetStars(PlayerPrefs.GetInt("levelStars" + (i + 1).ToString()));
        }
    }

    public void btnSoundOnOff()
    {
        bool b;
        if (PlayerPrefs.GetInt("pause") == 0)
        {
            PlayerPrefs.SetInt("pause", 1);
            b = true;
            ImageSoundButton.sprite = SoundEnabled;
        }
        else
        {
            PlayerPrefs.SetInt("pause", 0);
            b = false;
            ImageSoundButton.sprite = SoundDisabled;
        }
        GameObject.Find("Main Camera").GetComponent<AudioListener>().enabled = b;
    }

    void Awake()
    {
        PlayerPrefsLoad();
    }

    void Update()
    {
        ftimer += Time.unscaledDeltaTime;
    }
}
