﻿using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public float Life = 1.0f;
    public bool IsUnscaledTimer;

    void Update()
    {
        if (IsUnscaledTimer)
        {
            Life -= Time.unscaledDeltaTime;
        }
        else
        {
            Life -= Time.deltaTime;
        }
        
        if (Life < 0)
        {
            Destroy(gameObject);
        }
    }
}