﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonBuyCar : MonoBehaviour {

    public Text txt;
    public int typecar;
    public GameObject gogold;
    public int cost;
    public bool isopened;

    GameManager gm;

    void Awake()
    {
        gm = GameObject.Find("CanvasGame").GetComponent<GameManager>();
        if (PlayerPrefs.HasKey("hcar" + typecar.ToString()))
        {
            txt.text = "";
            gogold.SetActive(false);
            isopened = true;
        }
        else
        {
            txt.text = "BUY " + cost.ToString();
            isopened = false;
        }
    }

    public void BuyButton()
    {
        if (isopened)
        {
            gm.GameGUI.SetActive(true);
            gm.SwipeCamera.SetActive(true);
            GameObject.Find("Saver").GetComponent<Saver>().LoadFromShop(typecar);
            gm.PopupShop.SetActive(false);
        }
        else if (gm.ScoresOnCurrentLevel >= cost)
        {
            gm.ScoresOnCurrentLevel -= cost;
            PlayerPrefs.SetInt("scores", gm.ScoresOnCurrentLevel);
            PlayerPrefs.SetInt("hcar" + typecar.ToString(), 1);
            txt.text = "";
            gogold.SetActive(false);
            isopened = true;
        }
    }
}
