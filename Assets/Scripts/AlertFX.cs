﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AlertFX : MonoBehaviour {

    public float speed = 2.0f;
    public Color clr = Color.red;
    Text txt;
    float t;

    void Awake()
    {
        txt = GetComponent<Text>();
    }
	
	void Update () {
        if (Time.timeScale > 0)
        {
            t += Time.deltaTime * speed;
            if (t > 2.0f)
            {
                t = 1.0f;
                speed *= -1.0f;
            }
            if (t < 0.0f)
            {
                t = 0.0f;
                speed *= -1.0f;
            }
            txt.color = new Color(clr.r, clr.g, clr.b, t);
        }
        
	}
}
