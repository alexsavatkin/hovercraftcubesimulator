﻿using UnityEngine;
using System.Collections;

public class HCCube : MonoBehaviour
{
    public bool IsEngine;
    public float EnginePower = 1.0f;
    public GameObject init;
    public bool InGame;
    public bool IsMain;
    

    public Transform trmain;

    GameObject go;
    Rigidbody rb;
    public PlayerController plc;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void FindPlayerController(PlayerController plcc)
    {
        plc = plcc;
    }

    void OnTriggerEnter(Collider col)
    {
        if (InGame)
        {
            
            //if (!col.CompareTag("HCCube"))
            if (col.CompareTag("Bot"))
            {
                if (init != null)
                {
                    Instantiate(init, transform.position, transform.rotation);
                    //Debug.Log(name);
                    Destroy(gameObject);
                    /*
                    go = (GameObject)Instantiate(init, transform.position, transform.rotation);
                    rb = go.GetComponent<Rigidbody>();
                    rb.AddForce(Random.Range(50, 350) * (transform.position - trmain.position));
                    rb.AddTorque(Random.Range(-150, 150), Random.Range(-150, 150), Random.Range(-150, 150));
                    Destroy(gameObject);
                     */
                }
            }
        }
    }

    /*
    void OnCollisionEnter(Collision col)
    {
        
        if (IsMain)
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
        
    }
    */

    void OnDestroy()
    {
        if (InGame)
        {
            plc.kkt--;
            if (plc.kkt <= 2)
            {
                plc.gm.Lose();
            }
            if (InGame)
            {
                if (IsEngine)
                {
                    plc.k--;
                }
                //plc.DecK();
            }
        }
    }
}
