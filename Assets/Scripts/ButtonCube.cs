﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonCube : MonoBehaviour
{
    public int CubeType;
    public Image ImageButton;
    public Image ImageCube;
    Creator creator;

    void Start()
    {
        creator = GameObject.Find("Creator").GetComponent<Creator>();
        if (CubeType > 0)
        {
            ImageCube.color = creator.Colors[CubeType];
        }
    }

    public void Select()
    {
        creator.SelectCube(CubeType);  
    }
}
