﻿using UnityEngine;
using System.Collections;

public class ButtonUrl : MonoBehaviour
{
    public string url;

    public void OpenUrl()
    {
        Application.OpenURL(url);
    }
}
