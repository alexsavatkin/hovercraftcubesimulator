﻿using UnityEngine;
using System.Collections;

public class AnimRoom : MonoBehaviour
{
    public Material mat;
    public float speed = 1.0f;
    public float max = 0.9f;
    public float min = 0.3f;

    public float t;
    int dir = 1;
    float abs_max;
    float abs_min;

    void Awake()
    {
        abs_max = max;
        abs_min = min;
        t = min + Random.Range(-0.2f, 0.2f);
        dir = 1;
    }

    void Update()
    {
        if (dir == 1)
        {
            if (t < max)
            {
                t += speed * Time.deltaTime;   
            }
            else
            {
                max = abs_max + Random.Range(-0.2f, 0.2f);
                dir = -1;
            }
        }
        else if (dir == -1)
        {
            if (t > min)
            {
                t -= speed * Time.deltaTime;
            }
            else
            {
                min = abs_min + Random.Range(-0.2f, 0.2f);
                dir = 1;
            }
        }
        mat.SetColor("_EmissionColor", new Color(t, t, t, 1));
    }
}
