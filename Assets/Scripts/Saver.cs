﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Saver : MonoBehaviour
{
    public Creator creator;
    public GameObject[] CarFromShops;

    public void Save()
    {
        int slotNumber = creator.CurrentSlot;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo" + slotNumber.ToString() + ".dat");
        SavedData data = new SavedData();
        data.elements = new int[100];
        data.vsx = new float[100];
        data.vsy = new float[100];
        data.vsz = new float[100];
        data.length = creator.length;
        for (int i = 0; i < data.length; i++)
        {
            data.elements[i] = creator.elements[i];
            data.vsx[i] = creator.vs[i].x;
            data.vsy[i] = creator.vs[i].y;
            data.vsz[i] = creator.vs[i].z;
        }
        bf.Serialize(file, data);
        file.Close();
        creator.textDebug.gameObject.SetActive(true);
        creator.textDebug.text = "Saving Slot #" + creator.CurrentSlot.ToString();
        Debug.Log("Save file: " + Application.persistentDataPath + "/playerInfo" + slotNumber.ToString() + ".dat");
        creator.soundSave.Play();
    }

    public void Load()
    {
        int slotNumber = creator.CurrentSlot;
        if (File.Exists(Application.persistentDataPath + "/playerInfo" + slotNumber.ToString() + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo" + slotNumber.ToString() + ".dat", FileMode.Open);
            SavedData data = (SavedData)bf.Deserialize(file);
            file.Close();
            creator.ClearCar();
            creator.engines = 0;
            creator.length = data.length;
            for (int i = 0; i < creator.length; i++)
            {
                creator.elements[i] = data.elements[i];
                if (data.elements[i] == 7)
                {
                    creator.engines++;
                }
                creator.vs[i] = new Vector3(data.vsx[i], data.vsy[i], data.vsz[i]);
                GameObject g = (GameObject)Instantiate(creator.Cubes[creator.elements[i]], creator.vs[i], Quaternion.identity);
                g.transform.parent = creator.TransformCubeMain;
                creator.goscubes[i] = g;
            }
            //creator.goscubes[0] = GameObject.Find("CubeMain");
            creator.textDebug.gameObject.SetActive(true);
            creator.textDebug.text = "Load Slot #" + creator.CurrentSlot.ToString();
            Debug.Log("Load car");
            
        }
        else
        {
            creator.textDebug.gameObject.SetActive(true);
            creator.textDebug.text = "Error! Slot #" + creator.CurrentSlot.ToString() + " Is Empty";
            Debug.Log("Slot is empty");
        }
    }

    public void LoadFromShop(int num)
    {
        creator.ClearCar();
        creator.engines = 0;
        Time.timeScale = 1.0f;
        //GameObject g = (GameObject)Instantiate(CarFromShops[num], new Vector3(0, 5, 0), Quaternion.identity);
        CarFromShop carfromshop = CarFromShops[num].GetComponent<CarFromShop>();
        creator.length = carfromshop.goscubes.Length;
        Vector3 offsetv = carfromshop.transform.position;
        for (int i = 0; i < creator.length; i++)
        {
            Debug.Log("NAME=" + carfromshop.goscubes[i].name);
            creator.elements[i] = int.Parse(carfromshop.goscubes[i].name[5].ToString());
            if (creator.elements[i] == 7)
            {
                creator.engines++;
            }
            creator.vs[i] = carfromshop.goscubes[i].transform.position - offsetv + new Vector3(0, 5, 0);
            GameObject o = (GameObject)Instantiate(creator.Cubes[creator.elements[i]], creator.vs[i], Quaternion.identity);
            o.transform.parent = creator.TransformCubeMain;
            creator.goscubes[i] = o;
            /*
            creator.vs[i] = carfromshop.goscubes[i].transform.position;
            carfromshop.goscubes[i].transform.parent = creator.TransformCubeMain;
            creator.goscubes[i] = carfromshop.goscubes[i];
            */ 
        }
        //creator.goscubes[0] = GameObject.Find("CubeMain");
        creator.textDebug.gameObject.SetActive(true);
        creator.textDebug.text = "Load from Shop #" + num.ToString();
        Debug.Log("Load car");
    }

    [Serializable]
    class SavedData
    {
        public int length;
        public int[] elements;
        public float[] vsx;
        public float[] vsy;
        public float[] vsz;
    }
}