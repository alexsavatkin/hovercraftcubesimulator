﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public GameManager gm;
    public GameObject player;
    public float speed = 0;
    public Transform[] engines;
    public float v;
    public float h;
    public float vin;
    public float hin;
    public int height = 2;

    Rigidbody rb;
    Transform tr;
    GameObject[] gos;
    public int k;
    float tsc;
    Vector3 voft;
    public float kkt;
    public float fuel;
    public float fuelk;
    public Image fuelind;
    public GameObject txtFuelLow;

    void OnEnable()
    {
        voft = new Vector3(0, -1, 0);
        player = GameObject.Find("CubeMain");
        rb = player.GetComponent<Rigidbody>();
        tr = player.transform;
        gos = GameObject.FindGameObjectsWithTag("HCCube");
        kkt = gos.Length;
        fuel = 1.0f;
        
        k = 0;
        for (int i = 0; i < gos.Length; i++)
        {
            if (gos[i].GetComponent<HCCube>().IsEngine)
            {
                k++;
            }
        }
        engines = new Transform[k];
        k = 0;
        for (int i = 0; i < gos.Length; i++)
        {
            if (gos[i].GetComponent<HCCube>().IsEngine)
            {
                engines[k] = gos[i].transform;
                gos[i].GetComponent<SphereCollider>().enabled = true;
                k++;
            }
        }
        if (k == 1)
        {
            speed = 4.0f;
        }
        else if (k == 2)
        {
            speed = 5.0f;
        }
        else if (k == 3)
        {
            speed = 6.0f;
        }
        else if (k == 4)
        {
            speed = 6.5f;
        }
        else if (k >= 5)
        {
            speed = 7f;
        }
        else
        {
            gm.Lose();
        }
        fuelk = 0.5f / kkt - speed / 600.0f;
    }

    public void SetHIn(float t)
    {
        hin = t;
    }

    public void SetVIn(float t)
    {
        vin = t;
    }

    void LeftRightControll()
    {
        if (hin > 0)
        {
            h += Time.deltaTime;
            if (h > 1)
            {
                h = 1;
            }
        }
        else if (hin < 0)
        {
            h -= Time.deltaTime;
            if (h < -1)
            {
                h = -1;
            }
        }
        else
        {
            h *= 0.9f;
            if (Mathf.Abs(h) < 0.01f)
            {
                h = 0;
            }
        }
    }

    void UpDownControll()
    {
        if (vin > 0)
        {
            v += Time.deltaTime;
            if (v > 1)
            {
                v = 1;
            }
        }
        else if (vin < 0)
        {
            v -= Time.deltaTime;
            if (v < -1)
            {
                v = -1;
            }
        }
        else
        {
            v *= 0.95f;
            if (Mathf.Abs(v) < 0.01f)
            {
                v = 0;
            }
        }
    }

    int kt;
    bool lose;
    float ttt;

    void LateUpdate()
    {
        if (Time.timeScale > 0)
        {
            ttt += Time.deltaTime;
#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.UpArrow))
            {
                vin = 1;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                vin = -1;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                hin = -1;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                hin = 1;
            }
#endif
            LeftRightControll();
            UpDownControll();
            fuel -= Time.deltaTime * fuelk * 1.2f;
            if (fuel < 0.2f)
            {
                if (!txtFuelLow.activeSelf)
                {
                    txtFuelLow.SetActive(true);
                }
            }
            if (fuel <= 0)
            {
                fuel = 0;
                gm.Lose();
                lose = true;
            }
            fuelind.fillAmount = fuel;
            kt++;


            // RAYCAST

            RaycastHit rayhit;
            if (Physics.Raycast(tr.position + voft, voft, out rayhit, height - 1))
            {
                if (rayhit.collider.CompareTag("LoseBox"))
                {
                    if (!lose)
                    {
                        gm.Lose();
                    }
                }
                //Debug.DrawLine(tr.position + new Vector3(0, -1, 0), tr.position + new Vector3(0, -height, 0));
                tr.position = new Vector3(tr.position.x, rayhit.point.y + height, tr.position.z);
            }



            if (h != 0)
            {
                tr.Rotate(0, 50 * h * Time.deltaTime, 0);
                tr.Translate(new Vector3(h * Time.deltaTime * speed, 0, 0));
            }
            //tr.Translate(2 * speed * Time.deltaTime * tr.forward);
            tr.Translate(0, 0, 2 * speed * Time.deltaTime);

            Debug.DrawLine(tr.position, tr.position + 5 * tr.forward, Color.green);

            if (v != 0)
            {
                if (v < 0)
                {
                    v /= 2.0f;
                }
                tr.Translate(0, 0, speed * v * Time.deltaTime);
            }
            if (Time.timeScale == 1)
            {
                tsc += Time.deltaTime;
                if (tsc > 7.0f / speed)
                {
                    tsc = 0;
                    gm.ScoresOnCurrentLevel += 1;
                    if (v > 0.5f)
                    {
                        gm.ScoresOnCurrentLevel += 1;
                    }

                    if (k == 1)
                    {
                        speed = 6.0f;
                    }
                    else if (k == 2)
                    {
                        speed = 7.0f;
                    }
                    else if (k == 3)
                    {
                        speed = 8.0f;
                    }
                    else if (k == 4)
                    {
                        speed = 8.5f;
                    }
                    else if (k >= 5)
                    {
                        speed = 9f;
                    }
                    else
                    {
                        if (!lose)
                        {
                            gm.Lose();
                        }
                    }
                    speed += ttt / 30.0f;
                    fuelk = 0.7f / kkt - speed / 800.0f;
                }
            }
        }
    }

    public void AddFuel()
    {
        fuel += 0.25f;
        gm.SoundPlay("SoundCubeCreate");
    }

    public void DecK()
    {
        int cbs = GameObject.FindGameObjectsWithTag("HCCube").Length;
        Debug.Log("CUBES: " + cbs);
        if (cbs <= 1)
        {
            if (!lose)
            {
                gm.Lose();
            }
        }
    }
}
