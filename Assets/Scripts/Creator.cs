﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Creator : MonoBehaviour
{
    public GameObject[] Cubes;
    public Color[] Colors;
    public ButtonCube[] Buttons;
    public int[] CubeCounts;
    public Text[] CubeCountsText;
    public int CurrentCube = 0;
    public Color UnselectedCube = new Color(0, 0, 0, 0);
    public Color SelectedCube = Color.yellow;
    public Image ImageCurrent;
    public Transform TransormCamera;
    public Transform TransfotmTarget;
    public GameObject FaderImage;
    public float DoubleClickTime = 0.2f;
    public Transform SelectedCell;
    public float Grid = 2;
    public int CurrentSlot;
    public Transform TransformCubeMain;
    public Saver saver;
    public int engines;
    public int allcubes;

    public GameManager gm;

    float t;

    public GameObject[] goscubes;
    public int[] elements;
    public Vector3[] vs;
    public int length;
    public ButtonCube[] ButtonsSlots;


    public Text textDebug;

    GameObject[] gos;

    [Header("Звуки")]
    public AudioSource soundCubeCreate;
    public AudioSource soundCubeDelete;
    public AudioSource soundSave;
    public AudioSource musicInEditor;
    public AudioSource musicInGame;

    [Header("Тестовые объекты")]
    public Transform trCollPos;

    void Start()
    {
        gm = GameObject.Find("CanvasGame").GetComponent<GameManager>();
        textDebug = GameObject.Find("TextDebug").GetComponent<Text>();
        SelectCube(0);
        ClearCar();
        CurrentSlot = 0;
        saver.Load();
        musicInEditor.Play();
        for (int i = 0; i < CubeCounts.Length; i++)
        {
            CubeCounts[i] = PlayerPrefs.HasKey("cc" + i.ToString()) ? PlayerPrefs.GetInt("cc" + i.ToString()) : 0;
            PlayerPrefs.SetInt("cc" + i.ToString(), CubeCounts[i]);
            CubeCountsText[i].text = CubeCounts[i].ToString();
        }
        if (CubeCounts[0] <= 0)
        {
            CubeCounts[0] = 2;
            PlayerPrefs.SetInt("cc0", CubeCounts[0]);
            CubeCountsText[0].text = CubeCounts[0].ToString();
        }
    }

    public void BuyCubeN(int n)
    {
        if (n == 7)
        {
            if (gm.ScoresOnCurrentLevel >= 50)
            {
                gm.ScoresOnCurrentLevel -= 50;
                CubeCounts[n]++;
                PlayerPrefs.SetInt("cc" + n.ToString(), CubeCounts[n]);
                CubeCountsText[n].text = CubeCounts[n].ToString();
                PlayerPrefs.SetInt("scores", gm.ScoresOnCurrentLevel);
            }
        }
        else
        {
            if (gm.ScoresOnCurrentLevel >= 10)
            {
                gm.ScoresOnCurrentLevel -= 10;
                CubeCounts[n]++;
                PlayerPrefs.SetInt("cc" + n.ToString(), CubeCounts[n]);
                CubeCountsText[n].text = CubeCounts[n].ToString();
                PlayerPrefs.SetInt("scores", gm.ScoresOnCurrentLevel);
            }
        }
    }

    public void ClearCar()
    {
        gos = GameObject.FindGameObjectsWithTag("HCCube");
        for (int i = 0; i < gos.Length; i++)
        {
            Destroy(gos[i]);
        }
        elements = new int[300];
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i] = -1;    
        }
        vs = new Vector3[300];
        goscubes = new GameObject[300];
        length = 0;
        GameObject g = (GameObject)Instantiate(Cubes[7], new Vector3(0, 5, 0), Quaternion.identity);
        AddElement(7, g);
        g.name = "CubeMain";
        TransformCubeMain = g.transform;
        textDebug.gameObject.SetActive(true);
        textDebug.text = "All cubes deleted";
        soundSave.Play();
        engines = 1;
    }

    public void SelectCube(int n)
    {
        CurrentCube = n;
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].ImageButton.color = UnselectedCube;
        }
        Buttons[n].ImageButton.color = SelectedCube;
        ImageCurrent.color = Colors[n];
        soundCubeCreate.Play();
    }

    public void UpdateEnginesRecount()
    {
        engines = 0;
        for (int i = 0; i < length; i++)
        {
            if (goscubes[i].GetComponent<HCCube>().IsEngine)
            {
                engines++;
            }
        }
    }

    public void CreateCubeButton()
    {
        FaderImage.SetActive(true);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, LayerMask.NameToLayer("Cube")))
        {
            //trCollPos.position = hit.point;
            
            if (CurrentCube == 6)
            {

                Debug.DrawLine(ray.origin, hit.point);
                //Vector3 v = hit.point + (hit.point - new Vector3(0, 5, 0)).normalized * 0.2f + new Vector3(0, -0.2f, 0);
                DeleteElement(hit.transform.gameObject);
                soundCubeDelete.Play();
                engines = 0;
                for (int i = 0; i < gos.Length; i++)
                {
                    if (goscubes[i].GetComponent<HCCube>().IsEngine)
                    {
                        engines++;
                    }
                }
            }
            else
            {
                if (CubeCounts[CurrentCube] > 0)
                {
                    Debug.Log("Y " + hit.point.y);
                    if (hit.point.y < 6.4f && hit.point.y > 4.2f && hit.point.x > -2.4f && hit.point.x < 2.4f && hit.point.z > -2.4f && hit.point.z < 2.4f)
                    {
                        
                        CubeCounts[CurrentCube]--;
                        PlayerPrefs.SetInt("cc" + CurrentCube.ToString(), CubeCounts[CurrentCube]);
                        CubeCountsText[CurrentCube].text = CubeCounts[CurrentCube].ToString();
                        Vector3 v = hit.point;
                        //Grid = 0.4f;
                        trCollPos.position = v;
                        trCollPos.position += 0.1f * (trCollPos.position - hit.transform.position);
                        v = trCollPos.position;
                        trCollPos.position = new Vector3(Grid * Mathf.Round(v.x / Grid), -0.2f + Grid * Mathf.Round((0.2f + v.y) / Grid), Grid * Mathf.Round(v.z / Grid));
                        //SelectedCell.position = trCollPos.position;
                        //Debug.DrawLine(ray.origin, hit.point);
                        //Vector3 v = hit.point + (hit.point - new Vector3(0, 5, 0)).normalized * 0.2f + new Vector3(0, -0.2f, 0);
                        GameObject g = (GameObject)Instantiate(Cubes[CurrentCube], trCollPos.position, Quaternion.identity);
                        g.transform.parent = TransformCubeMain;
                        AddElement(int.Parse(g.name[5].ToString()), g);
                        soundCubeCreate.Play();
                        engines = 0;
                        for (int i = 0; i < length; i++)
                        {
                            if (goscubes[i].GetComponent<HCCube>().IsEngine)
                            {
                                engines++;
                            }
                        }
                    }
                    else
                    {
                        soundCubeDelete.Play();
                    }
                }
                else
                {
                    soundCubeDelete.Play();
                }
            }
             
        }
    }

    public void AddElement(int type, GameObject g)
    {
        if (length < 300)
        {
            goscubes[length] = g;
            elements[length] = type;
            vs[length] = g.transform.position;
            length++;
        }      
    }

    public void DeleteElement(GameObject g)
    {
        if (g.name != "CubeMain")
        {
            for (int i = 0; i < length; i++)
            {
                if (goscubes[i] == g)
                {
                    for (int j = i + 1; j < length; j++)
                    {
                        goscubes[j - 1] = goscubes[j];
                    }
                    length--;
                    i = length;
                    //
                    Destroy(g);
                }
            }
        }    
    }

    public void LoadCurrentCarInCurrentSlot(int n)
    {
        CurrentSlot = n;
        //saver.Load(CurrentSlot);
        textDebug.gameObject.SetActive(true);
        textDebug.text = "Slot #" + CurrentSlot.ToString() + " Selected";
        for (int i = 0; i < ButtonsSlots.Length; i++)
        {
            ButtonsSlots[i].ImageButton.color = Color.white;
        }
        ButtonsSlots[CurrentSlot - 1].ImageButton.color = Color.yellow;
        soundCubeCreate.Play();
    }

    public void SetTimeScale(float t)
    {
        Time.timeScale = t;
    }

    void Update()
    {
        if (Time.timeScale > 0)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, LayerMask.NameToLayer("Cube")))
            //if (Physics.Raycast(TransormCamera.position, , out hit, LayerMask.NameToLayer("Cube")))
            {
                Vector3 v = hit.point;
                //Grid = 0.4f;
                trCollPos.position = v;
                trCollPos.position += 0.5f * (trCollPos.position - hit.transform.position);
                v = trCollPos.position;
                trCollPos.position = new Vector3(Grid * Mathf.Round(v.x / Grid), -0.2f + Grid * Mathf.Round((0.2f + v.y) / Grid), Grid * Mathf.Round(v.z / Grid));
                SelectedCell.position = trCollPos.position;
                /*
                Vector3 v = hit.point + (hit.point - new Vector3(0, 5, 0)).normalized * 0.4f + new Vector3(0, -0.2f, 0);
                SelectedCell.position = new Vector3(Grid * Mathf.Round(v.x / Grid), 0.2f + Grid * Mathf.Round(v.y / Grid), Grid * Mathf.Round(v.z / Grid));
                Debug.DrawLine(ray.origin, v, Color.yellow);
                 */
            }
            else
            {
                SelectedCell.position = new Vector3(0, -100, 0);
            }

            t -= Time.deltaTime;
            if (t < 0)
            {
                t = 0;
            }
            if (Input.GetMouseButtonDown(0))
            {
                if (t > 0)
                {
                    CreateCubeButton();
                }
                t = DoubleClickTime;
            }
        }
    }
}
