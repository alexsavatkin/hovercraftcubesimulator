﻿using UnityEngine;
using System.Collections;

public class Optimizer : MonoBehaviour
{
    public float dist;
    public int steps;
    GameObject[] gos;

    int x;
    float d;
    float dx;
    float dy;

    void Start()
    {
        dist *= dist;
        gos = GameObject.FindGameObjectsWithTag("Tree");
        Debug.Log("Tree: " + gos.Length);
    }

    void Update()
    {
        for (int i = 0; i < steps; i++)
        {
            x++;

            if (x >= gos.Length)
            {
                x = 0;
            }

            dx = transform.position.x - gos[x].transform.position.x;
            dy = transform.position.z - gos[x].transform.position.z;
            d = dx * dx + dy * dy;

            if (d < dist)
            {
                if (!gos[x].activeSelf)
                {
                    gos[x].SetActive(true);
                }
            }
            else
            {
                if (gos[x].activeSelf)
                {
                    gos[x].SetActive(false);
                }
            }
        }
    }
}