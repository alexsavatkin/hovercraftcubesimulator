﻿using UnityEngine;
using System.Collections;

public class Bak : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("HCCube"))
        {
            GameObject.Find("PlayerController").GetComponent<PlayerController>().AddFuel();
            Destroy(gameObject);
        }
    }
}
